import React from 'react';
import AppRoutes from './routes';

import './Styles/global.css'

function App() {
  return (
   <AppRoutes/>
  );
}

export default App;
