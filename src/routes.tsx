import React from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';

import Landing from './Pages/Landing';
import PostsMap from './Pages/PostsMap';

function AppRoutes(){
    return(
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Landing/>}></Route>
                <Route path="/explore" element={<PostsMap/>}></Route>
            </Routes>
        </BrowserRouter>
    );
}

export default AppRoutes;