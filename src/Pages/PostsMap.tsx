import React from "react";

import logo from '../Images/logo.png'
import {TiSocialInstagram, TiSocialTwitter} from "react-icons/ti"
import {Link} from 'react-router-dom';
import { MapContainer, TileLayer} from 'react-leaflet';

import 'leaflet/dist/leaflet.css';

import '../Styles/Pages/posts-map.css'

function PostsMap(){
    return(
        <div id="page-map">
            <aside>
                <header>
                    <img src={logo} alt="Logo Futebol Geográfico" />

                    <h2>Escolha uma localização no mapa</h2>
                    <p>Cada ponto, uma história diferente</p>

                    <Link to="/sobre">Quem somos</Link>
                </header>

                <footer>
                    <a href="https://www.instagram.com/futebolgeografico/" target="_blank">
                        <TiSocialInstagram size={46} color="rgba(0, 0, 0, 0.6)"/>
                    </a>
                    <a href="https://twitter.com/futgeografico" target="_blank">
                        <TiSocialTwitter size={46} color="rgba(0, 0, 0, 0.6)"/>
                    </a>
                </footer>
            </aside>

            <MapContainer 
                center={[32.3662214,-5.499295]} 
                zoom={3} 
                style={{width: '100%', height: '100%'}}>
                {/*<TileLayer url="https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"></TileLayer>*/}
                <TileLayer 
                    url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`}
                />
            </MapContainer>
        </div>
    )
}

export default PostsMap;