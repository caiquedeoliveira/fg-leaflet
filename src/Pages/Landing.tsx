import React from 'react';

import '../Styles/Pages/landing.css'
import logo from '../Images/logo.png'

import {FiArrowRight} from 'react-icons/fi';
import {Link} from 'react-router-dom';

function Landing(){
    return(
        <div id="page-landing">
        <div className="content-wrapper">
          <div className="content-logo">
            <img src={logo} alt="Logo Futebol Geográfico" />
            <h1>Futebol Geográfico</h1>
          </div>
          
          <main>
            <h1>Produção de conteúdo na interface Futebol-Geografia</h1>
            <p>Professor-pesquisador na Geografia dos Esportes</p>
          </main>

          <Link to="/explore" className='enter-app'>
            <FiArrowRight size={26} color="rgba(0, 0, 0, 0.6)"/>
          </Link>
        </div>
      </div>
    )
}

export default Landing;